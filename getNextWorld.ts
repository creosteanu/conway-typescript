import {setSurvivors} from "./setSurvivors";
import {reproduce} from "./reproduce";
import {generateWorld} from "./generateWorld";

export function getNextWorld(world:World) : World {
    const nextWorld = generateWorld();

    setSurvivors(world, nextWorld);
    reproduce(world, nextWorld);

    return nextWorld;
}

import {getNeighbors} from "./getNeighbors";

export function reproduce(world: World, nextWorld: World) {
    for(let x = 0; x < world.length; x++){
        for(let y = 0; y < world[x].length; y++){
            if(world[x][y]) { continue; }

            const neighbors = getNeighbors(world, x, y);

            if(neighbors === 3){
                nextWorld[x][y] = true;
            }
        }
    }
}

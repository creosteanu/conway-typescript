import {initializeGameWorld} from "./initializeGameWorld";
import {logWorld} from "./logWorld";
import {getNextWorld} from "./getNextWorld";

const iterations = 10;

async function runGame(){
    let world = initializeGameWorld();
    await logWorld(world);

    for(let iteration = 0; iteration < iterations; iteration++){
       world = getNextWorld(world);
       await logWorld(world);
    }

}

runGame();

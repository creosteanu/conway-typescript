const xSize = 10;
const ySize = 10;

export function generateWorld() : World {
    const startingWorld = [];
    for (let x = 0; x < xSize; x++) {
        startingWorld[x] = [];
        for (let y = 0; y < ySize; y++) {
            startingWorld[x][y] = false;
        }
    }
    return startingWorld;
}

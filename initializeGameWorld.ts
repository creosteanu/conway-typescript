import * as startingLiveCells from "./startingLiveCells.json";
import {generateWorld} from "./generateWorld";

export function initializeGameWorld() : World {
    const startingWorld = generateWorld();

    for (const [x, y] of startingLiveCells) {
        startingWorld[x][y] = true;
    }

    return startingWorld;
}

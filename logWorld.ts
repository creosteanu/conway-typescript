export async function logWorld (world:World) {
    console.clear();
    for(const xLine of world) {
        let line = '';
        for(const cell of xLine){
            line = `${line}${cell ? '█' : ' '}`;
        }
        console.log(line);
    }

    return new Promise(resolve => setTimeout(resolve, 1000));
}

export function getNeighbors(world: World, x: number, y: number) : number {
    let number = 0;

    if(world[x-1] && world[x-1][y-1]) { number++; }
    if(world[x-1] && world[x-1][y]) { number++; }
    if(world[x-1] && world[x-1][y+1]) { number++; }
    if(world[x][y-1]) { number++; }
    if(world[x][y+1]) { number++; }
    if(world[x+1] && world[x+1][y-1]) { number++; }
    if(world[x+1] && world[x+1][y]) { number++; }
    if(world[x+1] && world[x+1][y+1]) { number++; }

    return number;
}
